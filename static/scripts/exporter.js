const hsl_srgb = ([h, s, l]) => {
    const c = ((s, l) => (1 - Math.abs(2 * l - 1)) * s)(s, l);
    const t = h / 60;
    const x = c * (1 - (t % 2 - 1));
    const m = l - c / 2;

    return ((c, x, h) => {
        if (h <= 1) { return [c, x, 0]; }
        else if (h <= 2) { return [x, c, 0]; }
        else if (h <= 3) { return [0, c, x]; }
        else if (h <= 4) { return [0, x, c]; }
        else if (h <= 5) { return [x, 0, c]; }
        else { return [c, 0, x]; }
    })(c, x, t).map((x) => x + m);
}

const srgb_lum = ([r, g, b]) => {
    const f = (x) => {
        if (x < 0.03928) {
            return x / 12.92;
        } else {
            return Math.pow((x + 0.055) / 1.055, 2.4);
        }
    };
    return 0.2126 * f(r) + 0.7152 * f(g) + 0.0722 * f(b);
};

const hsl_lum = (hsl) => srgb_lum(hsl_srgb(hsl));

const contrast = (lhs, rhs) => {
    const f = (l, r) => (l + 0.05) / (r + 0.05);
    if (lhs < rhs) {
        return f(rhs, lhs);
    } else {
        return f(lhs, rhs);
    }
}

const hsl_contrast = (lhs, rhs) => contrast(hsl_lum(lhs), hsl_lum(rhs));

const update_contrast = () => {
    const node_primary = document.getElementById("primary_contrast");
    const node_primary_alt = document.getElementById("primary-alt_contrast");
    const node_secondary = document.getElementById("secondary_contrast");
    const node_secondary_alt = document.getElementById("secondary-alt_contrast");
    const node_tertiary = document.getElementById("tertiary_contrast");

    if (node_primary && node_secondary && node_primary_alt && node_secondary_alt && node_tertiary) {
        const primary = [
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-hue")),
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-sat").slice(0, -1)) / 100,
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-lig").slice(0, -1)) / 100
        ];

        const secondary = [
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-hue")),
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-sat").slice(0, -1)) / 100,
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-lig").slice(0, -1)) / 100
        ];

        const tertiary = [
            Number(getComputedStyle(document.documentElement).getPropertyValue("--tertiary-hue")),
            Number(getComputedStyle(document.documentElement).getPropertyValue("--tertiary-sat").slice(0, -1)) / 100,
            Number(getComputedStyle(document.documentElement).getPropertyValue("--tertiary-lig").slice(0, -1)) / 100
        ];

        const primary_alt = [
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-alt-hue")),
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-alt-sat").slice(0, -1)) / 100,
            Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-alt-lig").slice(0, -1)) / 100
        ];

        const secondary_alt = [
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-alt-hue")),
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-alt-sat").slice(0, -1)) / 100,
            Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-alt-lig").slice(0, -1)) / 100
        ];

        const contrast = hsl_contrast(primary, secondary).toFixed(2);
        const contrast_alt = hsl_contrast(primary_alt, secondary_alt).toFixed(2);
        const contrast_ter = hsl_contrast(primary, tertiary).toFixed(2);
        const contrast_ter_alt = hsl_contrast(primary_alt, tertiary).toFixed(2);

        node_primary.textContent = " // Contrast with foreground: " + contrast + ":1 and " + contrast_ter + ":1 with accent.";
        node_secondary.textContent = " // Contrast with background: " + contrast + ":1";

        node_primary_alt.textContent = " // Contrast with foreground variant: " + contrast_alt + ":1 and " + contrast_ter_alt + ":1 with accent.";
        node_secondary_alt.textContent = " // Contrast with background variant: " + contrast_alt + ":1";

        node_tertiary.textContent = " // Contrast with background: " + contrast_ter + ":1 and variant: " + contrast_ter_alt + ":1.";
    }
};

const primary = () => {
    const node = document.getElementById("primary");
    if (node) {
        const hsl = getComputedStyle(document.documentElement).getPropertyValue("--primary");
        node.textContent = "--primary: " + hsl + ';';
    };

    primary_alt()
};

const primary_alt = () => {
    const node = document.getElementById("primary-alt");
    if (node) {
        node.textContent = "--primary-alt: " + getComputedStyle(document.documentElement).getPropertyValue("--primary-alt") + ';';
    }

    update_contrast();
};

const secondary = () => {
    const node = document.getElementById("secondary");
    if (node) {
        node.textContent = "--secondary: " + getComputedStyle(document.documentElement).getPropertyValue("--secondary") + ';'
    };

    secondary_alt()
};

const secondary_alt = () => {
    const node = document.getElementById("secondary-alt");
    if (node) {
        node.textContent = "--secondary-alt: " + getComputedStyle(document.documentElement).getPropertyValue("--secondary-alt") + ';';
    }

    update_contrast();
};

const tertiary = () => {
    const node = document.getElementById("tertiary");
    if (node) {
        node.textContent = "--tertiary: " + getComputedStyle(document.documentElement).getPropertyValue("--tertiary") + ';';
    }

    update_contrast();
};

const export_all = () => {
    primary();
    primary_alt();
    secondary();
    secondary_alt();
    tertiary();
};

document.addEventListener('DOMContentLoaded', () => { export_all() }, false);
