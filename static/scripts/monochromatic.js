const setPrimaryHue = (hue) => {
    document.documentElement.style.setProperty("--primary-hue", hue);
    localStorage.setItem("primaryHue", hue);

    primary();
    setSecondaryHue(hue);
}

const setPrimarySat = (sat) => {
    document.documentElement.style.setProperty("--primary-sat", sat + '%');
    localStorage.setItem("primarySat", sat);

    primary();
}

const setPrimaryLig = (lig) => {
    document.documentElement.style.setProperty("--primary-lig", lig + '%');
    localStorage.setItem("primaryLig", lig);

    primary();
}

const setPrimaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--primary-alt-sat", sat + '%');
    localStorage.setItem("primaryAltSat", sat);

    primary_alt();
}

const setPrimaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--primary-alt-lig", lig + '%');
    localStorage.setItem("primaryAltLig", lig);

    primary_alt();
}

const setSecondaryHue = (hue) => {
    document.documentElement.style.setProperty("--secondary-hue", hue);
    localStorage.setItem("secondaryHue", hue);

    secondary();
    setTertiaryHue(hue);
}

const setSecondarySat = (sat) => {
    document.documentElement.style.setProperty("--secondary-sat", sat + '%');
    localStorage.setItem("secondarySat", sat);

    secondary();
}

const setSecondaryLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-lig", lig + '%');
    localStorage.setItem("secondaryLig", lig);

    secondary();
}

const setSecondaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--secondary-alt-sat", sat + '%');
    localStorage.setItem("secondaryAltSat", sat);

    secondary_alt();
}

const setSecondaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-alt-lig", lig + '%');
    localStorage.setItem("secondaryAltLig", lig);

    secondary_alt();
}

const setTertiaryHue = (hue) => {
    document.documentElement.style.setProperty("--tertiary-hue", hue);
    localStorage.setItem("tertiaryHue", hue);

    tertiary();
}

const setTertiarySat = (sat) => {
    document.documentElement.style.setProperty("--tertiary-sat", sat + '%');
    localStorage.setItem("tertiarySat", sat);

    tertiary();
}

const setTertiaryLig = (lig) => {
    document.documentElement.style.setProperty("--tertiary-lig", lig + '%');
    localStorage.setItem("tertiaryLig", lig);

    tertiary();
}

const setTertiaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--tertiary-alt-sat", sat + '%');
    localStorage.setItem("tertiaryAltSat", sat);

    tertiary_alt();
}

const setTertiaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--tertiary-alt-lig", lig + '%');
    localStorage.setItem("tertiaryAltLig", lig);

    tertiary_alt();
}
