const localPrimaryHue = localStorage.getItem("primaryHue") || 200;
if (localPrimaryHue) { document.documentElement.style.setProperty("--primary-hue", Number(localPrimaryHue)); }
const localPrimarySat = localStorage.getItem("primarySat") || 30;
if (localPrimarySat) { document.documentElement.style.setProperty("--primary-sat", Number(localPrimarySat) + '%'); }
const localPrimaryLig = localStorage.getItem("primaryLig") || 90;
if (localPrimaryLig) { document.documentElement.style.setProperty("--primary-lig", Number(localPrimaryLig) + '%'); }

const localPrimaryAltSat = localStorage.getItem("primaryAltSat") || 40;
if (localPrimaryAltSat) { document.documentElement.style.setProperty("--primary-alt-sat", Number(localPrimaryAltSat) + '%'); }
const localPrimaryAltLig = localStorage.getItem("primaryAltLig") || 80;
if (localPrimaryAltLig) { document.documentElement.style.setProperty("--primary-alt-lig", Number(localPrimaryAltLig) + '%'); }


const localSecondaryHue = localStorage.getItem("secondaryHue") || 20;
if (localSecondaryHue) { document.documentElement.style.setProperty("--secondary-hue", Number(localSecondaryHue)); }
const localSecondarySat = localStorage.getItem("secondarySat") || 25;
if (localSecondarySat) { document.documentElement.style.setProperty("--secondary-sat", Number(localSecondarySat) + '%'); }
const localSecondaryLig = localStorage.getItem("secondaryLig") || 20;
if (localSecondaryLig) { document.documentElement.style.setProperty("--secondary-lig", Number(localSecondaryLig) + '%'); }

const localSecondaryAltSat = localStorage.getItem("secondaryAltSat") || 10;
if (localSecondaryAltSat) { document.documentElement.style.setProperty("--secondary-alt-sat", Number(localSecondaryAltSat) + '%'); }
const localSecondaryAltLig = localStorage.getItem("secondaryAltLig") || 35;
if (localSecondaryAltLig) { document.documentElement.style.setProperty("--secondary-alt-lig", Number(localSecondaryAltLig) + '%'); }

const localTertiaryHue = localStorage.getItem("tertiaryHue") || 230;
if (localTertiaryHue) { document.documentElement.style.setProperty("--tertiary-hue", Number(localTertiaryHue)); }
const localTertiarySat = localStorage.getItem("tertiarySat") || 65;
if (localTertiarySat) { document.documentElement.style.setProperty("--tertiary-sat", Number(localTertiarySat) + '%'); }
const localTertiaryLig = localStorage.getItem("tertiaryLig") || 50;
if (localTertiaryLig) { document.documentElement.style.setProperty("--tertiary-lig", Number(localTertiaryLig) + '%'); }
