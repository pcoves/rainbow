const setTriadic = (value) => {
    document.documentElement.style.setProperty("--triadic", value);
    localStorage.setItem("triadic", value);

    const hue = Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-hue"));
    setSecondaryHue((hue + value) % 360);
    setTertiaryHue((hue - value) % 360);
};

const flip = () => {
    setTriadic(Number(getComputedStyle(document.documentElement).getPropertyValue("--triadic")) * -1);
};

const setPrimaryHue = (hue) => {
    document.documentElement.style.setProperty("--primary-hue", hue);
    localStorage.setItem("primaryHue", hue);

    primary();
}

const setPrimarySat = (sat) => {
    document.documentElement.style.setProperty("--primary-sat", sat + '%');
    localStorage.setItem("primarySat", sat);

    primary();
}

const setPrimaryLig = (lig) => {
    document.documentElement.style.setProperty("--primary-lig", lig + '%');
    localStorage.setItem("primaryLig", lig);

    primary();
}

const setPrimaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--primary-alt-sat", sat + '%');
    localStorage.setItem("primaryAltSat", sat);

    primary_alt();
}

const setPrimaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--primary-alt-lig", lig + '%');
    localStorage.setItem("primaryAltLig", lig);

    primary_alt();
}

const setSecondaryHue = (hue) => {
    document.documentElement.style.setProperty("--secondary-hue", hue);
    localStorage.setItem("secondaryHue", hue);

    secondary();
}

const setSecondarySat = (sat) => {
    document.documentElement.style.setProperty("--secondary-sat", sat + '%');
    localStorage.setItem("secondarySat", sat);

    secondary();
}

const setSecondaryLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-lig", lig + '%');
    localStorage.setItem("secondaryLig", lig);

    secondary();
}

const setSecondaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--secondary-alt-sat", sat + '%');
    localStorage.setItem("secondaryAltSat", sat);

    secondary_alt();
}

const setSecondaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-alt-lig", lig + '%');
    localStorage.setItem("secondaryAltLig", lig);

    secondary_alt();
}

const setTertiaryHue = (hue) => {
    document.documentElement.style.setProperty("--tertiary-hue", hue);
    localStorage.setItem("tertiaryHue", hue);

    tertiary();
}

const setTertiarySat = (sat) => {
    document.documentElement.style.setProperty("--tertiary-sat", sat + '%');
    localStorage.setItem("tertiarySat", sat);

    tertiary();
}

const setTertiaryLig = (lig) => {
    document.documentElement.style.setProperty("--tertiary-lig", lig + '%');
    localStorage.setItem("tertiaryLig", lig);

    tertiary();
}

const localTriadic = localStorage.getItem("triadic") || 120;
if (localTriadic) { setTriadic(Number(localTriadic)); }
