const setPrimaryHue = (hue) => {
    document.documentElement.style.setProperty("--primary-hue", hue);
    localStorage.setItem("primaryHue", hue);

    setSecondaryHue();
    setTertiaryHue();

    primary();
}

const setPrimarySat = (sat) => {
    document.documentElement.style.setProperty("--primary-sat", sat + '%');
    localStorage.setItem("primarySat", sat);

    primary();
}

const setPrimaryLig = (lig) => {
    document.documentElement.style.setProperty("--primary-lig", lig + '%');
    localStorage.setItem("primaryLig", lig);

    primary();
}

const setPrimaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--primary-alt-sat", sat + '%');
    localStorage.setItem("primaryAltSat", sat);

    primary_alt();
}

const setPrimaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--primary-alt-lig", lig + '%');
    localStorage.setItem("primaryAltLig", lig);

    primary_alt();
}

const setSecondaryHue = () => {
    const prim = Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-hue"));
    const comp = Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-comp"));
    const hue = (prim + comp) % 360;

    document.documentElement.style.setProperty("--secondary-hue", hue);
    localStorage.setItem("secondaryHue", hue);

    secondary();
}

const setSecondarySat = (sat) => {
    document.documentElement.style.setProperty("--secondary-sat", sat + '%');
    localStorage.setItem("secondarySat", sat);

    secondary();
}

const setSecondaryLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-lig", lig + '%');
    localStorage.setItem("secondaryLig", lig);

    secondary();
}

const setSecondaryAltSat = (sat) => {
    document.documentElement.style.setProperty("--secondary-alt-sat", sat + '%');
    localStorage.setItem("secondaryAltSat", sat);

    secondary_alt();
}

const setSecondaryAltLig = (lig) => {
    document.documentElement.style.setProperty("--secondary-alt-lig", lig + '%');
    localStorage.setItem("secondaryAltLig", lig);

    secondary_alt();
}

const setTertiaryHue = () => {
    const prim = Number(getComputedStyle(document.documentElement).getPropertyValue("--primary-hue"));
    const comp = Number(getComputedStyle(document.documentElement).getPropertyValue("--tertiary-comp"));
    const hue = (prim + comp) % 360;

    document.documentElement.style.setProperty("--tertiary-hue", hue);
    localStorage.setItem("tertiaryHue", hue);

    tertiary();
}

const setTertiarySat = (sat) => {
    document.documentElement.style.setProperty("--tertiary-sat", sat + '%');
    localStorage.setItem("tertiarySat", sat);

    tertiary();
}

const setTertiaryLig = (lig) => {
    document.documentElement.style.setProperty("--tertiary-lig", lig + '%');
    localStorage.setItem("tertiaryLig", lig);

    tertiary();
}

const setSecondaryComp = (value) => {
    document.documentElement.style.setProperty("--secondary-comp", value);
    localStorage.setItem("secondaryComp", value);
    setSecondaryHue();
};

const setTertiaryComp = (value) => {
    document.documentElement.style.setProperty("--tertiary-comp", value);
    localStorage.setItem("tertiaryComp", value);

    setTertiaryHue();
};

const flipSecondary = () => {
    const comp = Number(getComputedStyle(document.documentElement).getPropertyValue("--secondary-comp"));
    setSecondaryComp((comp + 180) % 360);
};

const flipTertiary = () => {
    const comp = Number(getComputedStyle(document.documentElement).getPropertyValue("--tertiary-comp"));
    setTertiaryComp((comp + 180) % 360);
};

const localSecondaryComp = localStorage.getItem("secondaryComp") || 180;
if (localSecondaryComp) { setSecondaryComp(Number(localSecondaryComp)); }

const localTertiaryComp = localStorage.getItem("tertiaryComp") || 0;
if (localTertiaryComp) { setTertiaryComp(Number(localTertiaryComp)); }
