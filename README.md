# Rainbow 🌈

I'm bad at picking colors and make things look good.
But I'm good at making things work.

This is sad because people want both: it has to work and be nice.

Anyway, in an attempt to find a color palette for another side-project, I searched for *rules* to follow.
And I found some, and I made it work.
The rest is up to you at [https://pcoves.gitlab.io/rainbow](https://pcoves.gitlab.io/rainbow).
