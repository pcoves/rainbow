+++
title = "Triadic"
description = "Pick one `HUE` per third of the color space."
template = "pages/triadic.html"
weight = 3
+++

## Backgrounds

{{ primary() }}

{% wheel() %}
Pick the `HUE` and `saturation` for the **primary** color.
{% end %}

{% lightness() %}
And adjust the `lightness` for the **primary** color.
{% end %}

### Variant

{{ primary_alt() }}

{% saturation(alt = true) %}
Set the `saturation` for the **primary variant**.
{% end %}

{% lightness(alt = true) %}
And the `lightness` for the **primary variant**.
{% end %}

## Foregrounds

{{ secondary() }}

{% triads() %}
Pick the `HUE` and `saturation` for the **secondary** color.

Note that it determines the `HUE` for the **tertiary** color as well.
{% end %}

{% lightness(target = "secondary") %}
And the `lightness` for the **secondary** color.
{% end %}

### Variant

{{ secondary_alt() }}

{% saturation(target = "secondary", alt = true) %}
Set the `saturation` for the **secondary variant**.
{% end %}

{% lightness(target = "secondary", alt = true) %}
And the `lightness` for the **secondary variant**.
{% end %}

## Accent

{{ tertiary() }}

{% saturation(target = "tertiary") %}
Select the `saturation` for the **tertiary** color.
{% end %}

{% lightness(target = "tertiary") %}
And its `lightness`.
{% end %}
