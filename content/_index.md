+++
title = "Home"
description = "Dig for the Leprechaun's gold&nbsp;!"

sort_by = "weight"
+++

## Hello there :wave:

I'm [P. COVES](https://pcoves.gitlab.io) and I'm a software developer by trade.

The thing is, I make things work.
But I don't know how to make them pleasant to look at (yet).
And we live in a world where things have to be both functional and good-looking.

I like to write whatever I do, think, hack on my site.
But like everything, the content *may* be good, if the form is not, readers won't stick.

A couple of nights ago, I searched for some sort of *magic formula* to create well-balanced color schemes.
And, it turns out there are some out there.
Granted it's not :sparkles: awesome designer :sparkles: work but, hey, it kinda works.

But, the *feedback loop* was slow:
1. Moving cursors,
2. Copy/pasting code on my site,
3. Deciding whether it good enough or not,
4. Nop ? Got to *1.*.

So, I made this site.
The five templates [monochromatic](@/monochromatic.md), [analogous](@/analogous.md), [complementary](@/complementary.md), [triadic](@/triadic.md) and [compound](@/compound.md) allow you to see the result **live** on here.

This won't magically make me an :sparkles: awesome designer :sparkles: overnight.
But at least I can now pick my colors quickly and focus on the rest.
Like, finding a way to make this site mobile-friendly...

Anyway, hope this come in handy for someone one day.
Enjoy !
