+++
title = "Monochromatic"
description = "A single **HUE** : anything else is up to the **saturation** and **lightness**."
template = "pages/monochromatic.html"
weight = 0
+++

## Backgrounds

{{ primary() }}

{% strip() %}
Pick the `HUE` and `saturation` for the **primary** color.
{% end %}

{% lightness() %}
And adjust the `lightness` for the **primary** color.
{% end %}

### Variant

{{ primary_alt() }}

{% saturation(alt = true) %}
Set the `saturation` for the **primary variant**.
{% end %}

{% lightness(alt = true) %}
And the `lightness` for the **primary variant**.
{% end %}

## Foregrounds

{{ secondary() }}

{% saturation(target = "secondary") %}
Set the `saturation` for the **secondary** color.
{% end %}

{% lightness(target = "secondary") %}
And it `lighness`.
{% end %}

### Variant

{{ secondary_alt() }}

{% saturation(target = "secondary", alt = true) %}
Set the `saturation` for the **secondary variant**.
{% end %}

{% lightness(target = "secondary", alt = true) %}
And the `lightness` for the **secondary variant**.
{% end %}

## Accent

{{ tertiary() }}

{% saturation(target = "tertiary") %}
Select the `saturation` for the **tertiary** color.
{% end %}

{% lightness(target = "tertiary") %}
And its `lightness`.
{% end %}

{{ tertiary() }}
