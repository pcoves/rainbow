+++
title = "Compound"
description = "Split complementary in da place."
template = "pages/compound.html"
weight = 4
+++

## Backgrounds

{{ primary() }}

{% wheel() %}
Pick the `HUE` and `saturation` for the **primary** color.
{% end %}

{% lightness() %}
And adjust the `lightness` for the **primary** color.
{% end %}

### Variant

{{ primary_alt() }}

{% saturation(alt = true) %}
Set the `saturation` for the **primary variant**.
{% end %}

{% lightness(alt = true) %}
And the `lightness` for the **primary variant**.
{% end %}

## Foregrounds

{{ secondary() }}

{% compound() %}
Pick the `HUE` and `saturation` for the **secondary** color.

Note that it determines the `HUE` for the **tertiary** color as well.
{% end %}

{% lightness(target = "secondary") %}
And the `lightness` for the **secondary** color.
{% end %}

### Variant

{{ secondary_alt() }}

{% saturation(target = "secondary", alt = true) %}
Set the `saturation` for the **secondary variant**.
{% end %}

{% lightness(target = "secondary", alt = true) %}
And the `lightness` for the **secondary variant**.
{% end %}

## Accent

{{ tertiary() }}

{% saturation(target = "tertiary") %}
Select the `saturation` for the **tertiary** color.
{% end %}

{% lightness(target = "tertiary") %}
And its `lightness`.
{% end %}
